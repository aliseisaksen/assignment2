# README #
**********************
Alise Isaksen
ami325
**********************
Assignment 2
**********************
I chose to write my servlet using Eclipse. As such, my repository should show the Eclipse folder hierarchy.

For parsing, I elected to use JSoup and have included the requisite .jar in my /WEB-INF/lib folder.

Please note: I've tried to be thorough with my error messages, adding more than Devdatta asked for so that the graders may know the status of the servlet after submitting a new query.

Moreover, my project does not support simultaneous open sessions in different browsers. As far as I could tell, there was no way to edit the cookies in such a way (or to retrieve information from cookies in such a way) that allowed my program to distinguish one browser from another. Strictly, one session is allowed open at one time, even if two separate browsers are involved.