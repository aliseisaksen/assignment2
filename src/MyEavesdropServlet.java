import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.CookieStore;
import java.net.URLDecoder;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.SessionTrackingMode;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/*
 * Alise Isaksen
 * ami325
 * Modern Web Applications assignment2
 */


public class MyEavesdropServlet extends HttpServlet {
        
	// http://localhost:8080/assignment2/myeavesdrop?username=devdatta&session=start
	
	// http://localhost:8080/assignment2/myeavesdrop?type=meetings&project=barbican&year=2013
	// http://localhost:8080/assignment2/myeavesdrop?type=meetings&project=solum&year=2014
	// http://localhost:8080/assignment2/myeavesdrop?type=irclogs&project=%23heat
	// http://localhost:8080/assignment2/myeavesdrop?type=irclogs&project=%23heat&year=2013
	
	// http://localhost:8080/assignment2/myeavesdrop?username=devdatta&session=end
	
	// Map of all users and their history
    Map<String, List<String>> userHistory = new HashMap<String, List<String>>();
	    
    // Current user's history
    List<String> visitedURLs = new ArrayList<String>();
    
    String userName;
    
    // Array of Cookies
	Cookie[] cookies;
	
	// Workable Array of Cookies
	List<Cookie> ckList = new ArrayList<Cookie>();
	

	
	// Boolean to keep track of current sessions
	boolean sessionLocked = false;
	
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response) 
                        throws ServletException, IOException
        {               
        	
        	cookies = request.getCookies();
        	 	
        	// Convert cookies[] to List for additional functionality
        	if(cookies != null)
        		ckList = Arrays.asList(cookies);

            // Retrieve and parse Query String
            String queryString = request.getQueryString();
            Map<String, String> queryMap = splitQuery(queryString);
           
            // Ensure that Query String is valid
            if(!verifyQuery(queryMap, response)){
            	response.getWriter().println("Invalid Query. Try again.");
            	return;
            }

            
            /***************************************************************************
              							Start / End a Session
             ***************************************************************************/
    		if(queryMap.containsKey("username")) {
    			
    			String username = queryMap.get("username");
    			String session = queryMap.get("session");
    			
    			if(session.equals("start")) {

    				// Ensure that the user session is not locked
    				// Add username to userHistory if it does not exist already
    				if( (!sessionLocked) && (!userHistory.containsKey(username)) ) {
    					
    					// Create userHistory session for username
    					userHistory.put(username, visitedURLs);
   					
    					// Create username cookie and add to response
        				Cookie cookie = new Cookie(username,"friend"); // ^^^ ?
                        cookie.setDomain("localhost");
                        cookie.setPath("/assignment2" + request.getServletPath());
                        cookie.setMaxAge(1000);
                        response.addCookie(cookie);
                        userName = username;
                        sessionLocked = true;
                        response.getWriter().println("Created new session for user: " + username);

    				} else {
    					if(sessionLocked || (userHistory.containsKey(username)) )
    						response.getWriter().println("Cannot add another session. An active session already exists"); 					
    					else
    	    				response.getWriter().println("Unknown error: line 100"); //?

    				}
    				
    			} else if(session.equals("end")) {
    				
    				// Verify username exists and remove it from userHistory
    				if(userHistory.containsKey(username)) {
    					
    					// Clear cookies; does not work...
//    					for(int i = 0; i < cookies.length; i++) {
//    						if (cookies[i].getName().equals(username)) {
//    							cookies[i].setDomain("localhost");
//    	                        cookies[i].setPath("/assignment2" + request.getServletPath());
//    							cookies[i].setMaxAge(0);
//    							cookies[i].setValue("strangerr");
//    							response.getWriter().println("Cookie removed.");
//    						}
//    					}
    					
    					// Clear visitedURLs and remove username from History
    					userHistory.get(username).clear();
    					userHistory.remove(username);
    					sessionLocked = false;
    					username = null;
    					userName = null;
    					
    					response.getWriter().println("Username session removed user history cleared.");
    					
    				} else 
    					response.getWriter().println("Cannot remove user: username did not exist as a session.");

    			} else 
    				response.getWriter().println("Invalid Session value: must use <start> or <end>");
    			
    			return;
    			
    		} 

        
            /***************************************************************************
										Handling Query Requests
             ***************************************************************************/
    		
        	// Receive Query String
    		// Verify the queryString contains a type and that a user session exists for the cookie involved
    		if(cookies != null && queryMap.containsKey("type") && userHistory.containsKey(userName)) {       		
    			
    			// Print Session History
    			if(userHistory.containsKey(userName)) {
    				response.getWriter().println("Visited URLs");

    				List<String> history = userHistory.get(userName);

    				for(int i = 0; i < history.size(); i++) {
             			response.getWriter().println(history.get(i));
             		}

             		response.getWriter().println();
    			}
		             		        		
    		} 
    		
    		else {
        		response.getWriter().println("Cookie or session for current user not identified");
         	}
        		 try {	
        			 
        			// Access requested URL and print data
                 	String source = "http://eavesdrop.openstack.org/";

                 	// Build URL
                 	source += "/" + queryMap.get("type");
                 	if(queryMap.containsKey("project"))
                 		source += "/" + queryMap.get("project");
                 	if(queryMap.containsKey("year"))
                 		source += "/" + queryMap.get("year");

                 	response.getWriter().println("URL Data");
                 	Document doc = Jsoup.connect(source).get();
         		    Elements links = doc.select("a");
         		    
         		    ListIterator<Element> iter = links.listIterator();
         		    
         		    for(int j = 0; j < 5; j++)
         		    {
         		    	iter.next();
         		    }
         		    
         		    while(iter.hasNext()) {
     		    		Element e = (Element) iter.next();
     		    		String s = e.html();
     		    		s = s.replace("#", "%23");
     		    		response.getWriter().println(source + s);
         		    }
         		    
         		    
         		   // Add URL to Session History
         		   if(userHistory.containsKey(userName))
         			   (userHistory.get(userName)).add(source);
         		    
         		} catch(Exception exp) {
         			exp.printStackTrace();
         		}
         		
       		

        }
        
       
        // from SO: http://stackoverflow.com/questions/13592236/parse-the-uri-string-into-name-value-collection-in-java
        public static Map<String, String> splitQuery(String queryString) throws UnsupportedEncodingException {
           
        	Map<String, String> queryMap = new LinkedHashMap<String, String>(); // Linked? Hash?
            String[] pairs = queryString.split("&");
            
            for(String pair : pairs) {
                int index = pair.indexOf("=");
                
                queryMap.put(pair.substring(0, index), pair.substring(index + 1));
                
            }
            
            return queryMap;
        }
        
        public static boolean verifyQuery(Map<String, String> queryMap, HttpServletResponse response) {

        	try {
				// TODO The name must conform to RFC 2109. That means it can contain only ASCII alphanumeric characters and cannot contain commas, semicolons, or white space or begin with a $ character.
	        	
        		// If username exists, then session must exist with only allowed values of start and end
        		if (queryMap.containsKey("username")) {
	        		if(queryMap.containsKey("session")) {
	        			if(!(queryMap.get("session").equals("start")) && !(queryMap.get("session").equals("end"))) {

	        				response.getWriter().println("Disallowed value specified for parameter session");
	        				return false;
	        			}
	        		} else {
	        			response.getWriter().println("Invalid input: Session value must be provided with username parameter");
	        			response.getWriter().println("Example: ?username=<value>&session=<value>");
	        			return false;
	        		}
	        	}
	        	
        		// If session exists, then username should exist and cannot be only spaces
	        	if (queryMap.containsKey("session")) {
	        		if(queryMap.containsKey("username")) {
	        			String username = queryMap.get("username");
	        			
	        			// Check if username contains only spaces -- does not work for URL encoded spaces
	        			if(username.trim().isEmpty()) {
	        				response.getWriter().println("Disallowed value specified for parameter username");
	        				return false;
	        			}
	        			
	        			String decoded = java.net.URLDecoder.decode(username, "UTF-8");
	        			if (decoded.trim().isEmpty()) {
	        				response.getWriter().println("Disallowed value specified for parameter username");
	        				return false;
	        			}
	        				
	        		} else {
	        			response.getWriter().println("Invalid input: Username value must be provided with session parameter");
	        			response.getWriter().println("Example: ?username=<value>&session=<value>");
	        			return false;
	        		}
	        	}
	        	
	        	// If type exists, then it should only contain either irclogs or meetings
	        	if (queryMap.containsKey("type")) {
	        		if(!(queryMap.get("type").equals("irclogs")) && !(queryMap.get("type").equals("meetings"))) {
	        			response.getWriter().println("Disallowed value specified for parameter type");
        				return false;
	        		}
	        	}

	        	// If project exists, then type should exist and project should only contain names from either /irclogs or /meetings
	        	if (queryMap.containsKey("project")) {
	        		if(queryMap.containsKey("type")) {
	        			
	        			if(queryMap.get("type").equals("irclogs")) {
		        			
	        				List<String> projNames = new ArrayList<String>();
	        				String source = "http://eavesdrop.openstack.org/irclogs";
		        			Document doc = Jsoup.connect(source).get();
		         		    Elements links = doc.select("a");
		         		    
		         		    ListIterator<Element> iter = links.listIterator();
		         		    
		         		    // safe?
		         		    for(int j = 0; j < 5; j++)
		         		    {
		         		    	iter.next();
		         		    }
		         		    
		         		    while(iter.hasNext()) {
	         		    		Element e = (Element) iter.next();
	         		    		String s = e.html();
	         		    		s = s.replace("#", "%23");
	         		    		s = s.substring(0, s.length()-1);
	         		    		projNames.add(s);
	         		    	}
		         		   
//		         		  
		         		    if(!projNames.contains(queryMap.get("project"))) {
		         		    	response.getWriter().println("Disallowed value specified for parameter project");
		        				return false;
		         		    }
		         		   
		         		    
	        			} else if(queryMap.get("type").equals("meetings")) {
	        				
	        				List<String> projNames = new ArrayList<String>();
	        				String source = "http://eavesdrop.openstack.org/meetings";
		        			Document doc = Jsoup.connect(source).get();
		         		    Elements links = doc.select("a");
		         		    
		         		    ListIterator<Element> iter = links.listIterator();
		         		    
		         		    // safe? think so
		         		    for(int j = 0; j < 5; j++)
		         		    {
		         		    	iter.next();
		         		    }
		         		    
		         		    while(iter.hasNext()) {
	         		    		Element e = (Element) iter.next();
	         		    		String s = e.html();
	         		    		s = s.replace("#", "%23");
	         		    		s = s.substring(0, s.length()-1);
	         		    		projNames.add(s);
		         		    }

		         		    if(!projNames.contains(queryMap.get("project"))) {
		         		    	response.getWriter().println("Disallowed value specified for parameter project");
		        				return false;
		         		    }
		         		   
	        			}
	        			
	        		} else {
	        			response.getWriter().println("Invalid input: type value must be provided with project parameter");
	        			response.getWriter().println("Example: ?type=<value>&project=<value>");
	        			return false;
	        		}
	        	}
	        	
	        	// If year exists, then type and project must exist
	        	// Check that year values = 2010, 2011, 2012, 2013, 2014, 2015
	        	if(queryMap.containsKey("year")) {
	        		if(queryMap.containsKey("type") && queryMap.containsKey("project")) {
	        			// Check that years are valid
	        			List<String> validYears = Arrays.asList("2010", "2011", "2012", "2013", "2014", "2015");
	        			
	        			if(validYears.contains(queryMap.get("year"))) {
	        				
	        			} else {
	        				response.getWriter().println("Disallowed value specified for parameter year");
	        				return false;
	        			}
	        		} else {
	        			response.getWriter().println("Invalid input: type and project value must be provided with year parameter");
	        			response.getWriter().println("Example: ?type=<value>&project=<value>&year=<value>");
	        			return false;
	        		}
	        	}

	        	
	        } catch(Exception exp) {
	 			exp.printStackTrace();
	 		}
        	return true;
        	
        }
        
}